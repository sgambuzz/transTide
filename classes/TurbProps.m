classdef TurbProps < handle
	%TURBPROPS holds the aerodynamic properties of the full blade
	% example: turbine = TurbProps(geomFile, aeroFile)
	% geomFile contains the blade geometry as a table, having at least the
	% following:
	%   * blades
	%   * rad_coord
	%   * twist
	%   * chord
	%
	% polarFile contains the following
	%  * rad_coord
	%  * foil (links to a file, path is relative from bladeFile)

	% as a rule:
	% changes in angle of attack are along the column (first index)
	% changes in span are along the rows (second index)
	% that means, LiftCoeff(i, j) is at the i-th AoA and the j-th radius

	properties
		% geometrical quantities
		Blades;
		RadialCoords;
		BladeChord;
		BladeTwist;
		Pitch = 0;

		% aerodynamic quantities
		AeroFoils;
		AngleOfAttack;
		LiftCoeff;
		DragCoeff;
		DragCoeff2D;
		NormCoeff;
		Grid;
		
		% stall delay
		Separation;
		ZeroLiftAngle;
		ZeroLiftDrag;
		LinearLiftSlope;
		LinearClRange;

		% booleans
		isExtrapolatedTo360 = false;
		hasGrids = false;
		hasRotationalAugmentation = false;
	end
	
	methods
		function obj = TurbProps(geometryFile, polarFile)
			% blade geometry
			fname = [geometryFile, '.csv'];
			turbineTab = readtable(fname, 'Delimiter', ',');
			obj.RadialCoords = turbineTab.rad_coord(:).';
			obj.BladeChord = turbineTab.chord(:).';
			obj.BladeTwist = turbineTab.twist(:).';
			obj.Blades = turbineTab.blades(1);

			% blade polars
			fname = [polarFile, '.csv'];
			folder = fileparts(fname);
			polarTab = readtable(fname, 'delimiter', ',');
			for jj = 1:height(polarTab)
				foilfile = polarTab.foil{jj};
				foilfile = replace(foilfile, {'/', '\'}, filesep);
				foils(jj) = AerofoilProps([folder, filesep, foilfile]);
				foils(jj).RadialCoord = polarTab.rad_coord(jj);
			end
			if length(foils) == 1
				% single-polar blade, cheat by redefining this as two
				% identical foils at hub and tip
				foils(2) = AerofoilProps([folder, filesep, foilfile]);

				foils(1).RadialCoord = obj.RadialCoords(1);
				foils(2).RadialCoord = obj.RadialCoords(end);
			end
			obj.AeroFoils = foils;

			% as we've changed the number of sections, we have to recompute
			% the polars over the blade span
			obj.makeAeroMatrices;
		end % class constructor

		function setDiscretisation(obj, n)
			%SETDISCRETISATION sets the number of sections to use to
			% discretise the blade
			rnew = linspace(obj.RadialCoords(1), obj.RadialCoords(end), n);
			cnew = interp1(obj.RadialCoords, obj.BladeChord, rnew, 'pchip');
			tnew = interp1(obj.RadialCoords, obj.BladeTwist, rnew, 'pchip');

			obj.RadialCoords = rnew;
			obj.BladeChord = cnew;
			obj.BladeTwist = tnew;

			obj.makeAeroMatrices;
		end % setDiscretisation

		function setPitchAngle(obj, newPitch)
			%SETPITCHANGLE sets the pitch angle of the blade, in absolute
			% terms
			oldPitch = obj.Pitch;
			obj.BladeTwist = obj.BladeTwist - oldPitch + newPitch;
			obj.Pitch = newPitch;
		end % setPitchAngle
		
		function makeAeroMatrices(obj)
			%MAKEAEROMATRICES makes the matrices of LiftCoeff, DragCoeff, 
			% NormCoeff and all quantities relevant to the separation and
			% stall delay
			alpha = obj.AeroFoils(1).AngleOfAttack(:);

			% prealloc
			r = zeros(1, length(obj.AeroFoils));
			Cl = zeros(length(alpha), length(obj.AeroFoils));
			Cd = zeros(length(alpha), length(obj.AeroFoils));
			Cn = zeros(length(alpha), length(obj.AeroFoils));
			a0 = zeros(1, length(obj.AeroFoils));
			cdcl0 = zeros(1, length(obj.AeroFoils));
			clin = zeros(1, length(obj.AeroFoils));
			crange = zeros(2, length(obj.AeroFoils));

			for jj = 1:length(obj.AeroFoils)
				r(jj) = obj.AeroFoils(jj).RadialCoord;
				Cl(:, jj) = obj.AeroFoils(jj).LiftCoeff;
				Cd(:, jj) = obj.AeroFoils(jj).DragCoeff;
				Cn(:, jj) = obj.AeroFoils(jj).NormCoeff;
				a0(jj) = obj.AeroFoils(jj).ZeroLiftAngle;
				cdcl0(jj) = obj.AeroFoils(jj).ZeroLiftDrag;
				clin(jj) = obj.AeroFoils(jj).LinearLiftSlope;
				crange(:, jj) = obj.AeroFoils(jj).LinearClRange(:);
			end

			Cl = interp2(r, alpha, Cl, obj.RadialCoords, alpha);
			Cd = interp2(r, alpha, Cd, obj.RadialCoords, alpha);
			Cn = interp2(r, alpha, Cn, obj.RadialCoords, alpha);
			a0 = interp1(r, a0, obj.RadialCoords);
			cdcl0 = interp1(r, cdcl0, obj.RadialCoords);
			clin = interp1(r, clin, obj.RadialCoords);
			crange = interp1(r, crange.', obj.RadialCoords).';

			% store
			obj.AngleOfAttack = alpha;
			obj.LiftCoeff = Cl;
			obj.DragCoeff = Cd;
			obj.NormCoeff = Cn;
			
			obj.ZeroLiftAngle = a0;
			obj.ZeroLiftDrag = cdcl0;
			obj.LinearLiftSlope = clin;
			obj.LinearClRange = crange;
		end	% makeAeroMatrices

		function applyRotationalAugmentation(obj)
			F = zeros(size(obj.NormCoeff));
			for jj = 1:length(obj.RadialCoords)
				F(:, jj) = sepPoint(obj.AngleOfAttack, obj.ZeroLiftAngle(jj), ...
					obj.NormCoeff(:, jj), obj.LinearLiftSlope(jj), ...
					obj.LinearClRange(:, jj));
			end
			[Cl_3d, Cd_3d, Cn_3d] = stallDelayMult(...
				obj.BladeTwist, obj.RadialCoords, obj.BladeChord, ...
				obj.AngleOfAttack, F, obj.LiftCoeff, obj.DragCoeff, ...
				obj.LinearLiftSlope, obj.ZeroLiftAngle);
			
			% keep old Cd_2d for solution at r > 0.8 rtip
			obj.DragCoeff2D = obj.DragCoeff;
			
			% store			
			obj.LiftCoeff = Cl_3d;
			obj.DragCoeff = Cd_3d;
			obj.NormCoeff = Cn_3d;
			obj.hasRotationalAugmentation = true;
		end % applyRotationalAugmentation

		function extrapPolars(obj)
			%EXTRAPPOLARS extrapolate polars to 360 degrees with Viterna
			% method
			for jj = size(obj.LiftCoeff, 2):-1:1
				[alpha, Cl(:, jj), Cd(:, jj), Cn(:, jj)] = ...
					vitExtrapolation(obj.AngleOfAttack, ...
					obj.LiftCoeff(:, jj), obj.DragCoeff(:, jj));
			end
			if obj.hasRotationalAugmentation
				% must extrapolate also Cd2d
				for jj = size(obj.LiftCoeff, 2):-1:1
					[~, ~, Cd2d(:, jj), ~] = ...
						vitExtrapolation(obj.AngleOfAttack, ...
						obj.LiftCoeff(:, jj), obj.DragCoeff2D(:, jj));
				end
			end
			
			% store
			obj.AngleOfAttack = alpha;
			obj.LiftCoeff = Cl;
			obj.DragCoeff = Cd;
			obj.NormCoeff = Cn;
			if obj.hasRotationalAugmentation
				obj.DragCoeff2D = Cd2d;
			end

			% as we've extrap the normal coefficient, recompute separation
			for jj = size(obj.LiftCoeff, 2):-1:1
				F(:, jj) = sepPoint(obj.AngleOfAttack, obj.ZeroLiftAngle(jj), ...
					obj.NormCoeff(:, jj), obj.LinearLiftSlope(jj), ...
					obj.LinearClRange(:, jj));
			end
			obj.Separation = F;
			obj.isExtrapolatedTo360 = true;
		end % extrapPolars

		function makeGrids(obj)
			if ~obj.isExtrapolatedTo360
				obj.extrapPolars;
			end
			obj.Grid.FCl = griddedInterpolant({obj.AngleOfAttack, obj.RadialCoords}, ...
				obj.LiftCoeff, 'linear', 'linear');
			obj.Grid.FCd = griddedInterpolant({obj.AngleOfAttack, obj.RadialCoords}, ...
				obj.DragCoeff, 'linear', 'linear');
			obj.Grid.F = griddedInterpolant({obj.AngleOfAttack, obj.RadialCoords}, ...
				obj.Separation, 'linear', 'linear');
			if obj.hasRotationalAugmentation
				obj.Grid.Cd2d = griddedInterpolant({obj.AngleOfAttack, obj.RadialCoords}, ...
					obj.DragCoeff2D, 'linear', 'linear');
			else
				obj.Grid.Cd2d = obj.Grid.FCd;
			end
			obj.hasGrids = true;
		end % makeGrids
	end % methods
end % class

