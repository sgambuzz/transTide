# transTide

This repo contains a fork of Gabriel Scarlett's transTide project (https://github.com/gabscarlett/transTide), adapted to blade geometries with different polars along their span.

## Tidal Benchmarking Project
This contains data for the Tidal Benchmarking Project of Supergen ORE (see https://supergen-ore.net/projects/tidal-turbine-benchmarking).
Scripts are included in the folder ```runscripts``` to generate the data for the clean and turublent test cases.

To generate the files of the submission, run the ```runscripts/make.m``` file.

## New features with respect to gabscarlett/transTide:
 * Introduced a new class, ```TurbProps``` to hold the geometry and the aerodynamic properties of the blades used.
 * To define this, use two tables (see in ```data/TidalBenchProj``` for examples).

## Bugfixes from Gabriel Scarlett's version:
 * Fixed a bug on ```bladeEM.m``` (computation of induction factors) that made the code converge to the wrong value of phi close to the blade root
 * Fixed a bug on ```RunConditions.m``` for which the code would ignore the user input on the shear law exponent
 * Fixed a bug on ```TidalSim.m``` for which the code would ignore the user input on the number of sections to discretise the blade into.
