function [a, ap] = bladeEM(U0, tsr, B, r, c, beta, polar)
%  [a, ap] = BLADEEM(U0, tsr, B, r, c, beta)
%  Compute the solution of the BEM problem for the blade fiven at oeprating tsr
%  This is an adaptation of the code 'ning-bem' (github.com/g102/ning-bem),
%  which has been modified to be a drop-in replacement for bladeEM (authored by
%  Gabriel Scarlett).
%
%  v2: fixed a bug that made this function converge to wrong values of phi
%  v3: extended to multiple polars along the blade
%  v4: changed anonymous functions to nested functions, now ≈ 2× faster
%
%  Inputs:
%    - U0: unused, kept only for compatibility reasons (DO NOT REMOVE)
%    - tsr: operating tip-speed ratio
%    - B: number of blades (used to compute tip- and root-losses)
%    - r: radial discretisation of the blade
%    - c: chord at the radial locations
%    - beta local twist (RADIANS)
%    - polar: struct containing FCl and FCd (call as FCl(a, r) or FCl(a))
%
%  Outputs:
%    - a: axial induction factor across the turbine blade
%    - ap: tangential induction factor across the turbine blade

	eps = 1e-6;
	
	% assuming: r is sorted from root to tip
	rhub = r(1);
	rtip = r(end);
	
	% geometrical functions
	solid = griddedInterpolant(r, B*c ./ (2*pi*r));
	beta = griddedInterpolant(r, beta);
	
	% solution of the BEM problem
	a = nan(size(r));
	ap = nan(size(r));
	for isec = 1:length(r)
		if f(pi/2, r(isec)) > 0
			phistar = zero(eps, pi/2, eps, eps, @(x) f(x, r(isec)));
		elseif fpb(-pi/4, r(isec)) > 0 && fpb(eps, r(isec)) > 0
			phistar = zero(-pi/4, -eps, eps, eps, @(x) fpb(x, r(isec)));
		else
			phistar = zero(pi/2, pi, eps, eps, @(x) f(x, r(isec)));
		end
		
		a(isec) = af(phistar, r(isec));
		ap(isec) = apf(phistar, r(isec));
	end

	% nested functions
	function y = F(phi, r)
		ftip = B/2 .* (rtip - r) ./ (r .* sin(phi));
		Ftip = 2/pi * acos(exp(-ftip));
		fhub = B/2 .* (r - rhub) ./ (r .* sin(phi));
		Fhub = 2/pi * acos(exp(-fhub));
		y = Ftip * Fhub;
	end
	
	function y = cnf(phi, r)
		aloc = phi - beta(r);
		if length(polar.FCl.GridVectors) == 2
			y = polar.FCl(aloc, r) .* cos(phi) + polar.FCd(aloc, r) .* sin(phi);
		else
			y = polar.FCl(aloc) .* cos(phi) + polar.FCd(aloc) .* sin(phi);
		end
	end

	function y = ctf(phi, r)
		aloc = phi - beta(r);
		if length(polar.FCl.GridVectors) == 2
			y = polar.FCl(aloc, r) .* sin(phi) - polar.FCd(aloc, r) .* cos(phi);
		else
			y = polar.FCl(aloc) .* sin(phi) - polar.FCd(aloc) .* cos(phi);
		end
	end
	
	function y = k(phi, r)
		y = solid(r) .* cnf(phi, r) ./ (4 * F(phi, r) .* (sin(phi)).^2);
		if isinf(y)
			y = realmax;
		end
	end

	function y = kp(phi, r)
 		y = solid(r) .* ctf(phi, r) ./ (4 * F(phi, r) .* sin(phi) .* cos(phi));
		if isinf(y)
			y = -realmax;
		end
	end
	
	function y = gamma1(phi, r)	
		y = 2 * F(phi, r) .* k(phi, r) - (10/9 - F(phi, r));
	end
	
	function y = gamma2(phi, r)
		y = 2 * F(phi, r) .* k(phi, r) - F(phi, r) .* (4/3 - F(phi, r));
	end
	
	function y = gamma3(phi, r)
		y = 2 * F(phi, r) .* k(phi, r) - (25/9 - 2 * F(phi, r));
	end
	
	function y = af(phi, r)
		if k(phi, r) <= 2/3
			y = k(phi, r)./(1 + k(phi, r));
		else
			y = (gamma1(phi, r) - sqrt(gamma2(phi, r))) ./ gamma3(phi, r);
		end
	end
	
	function y = apf(phi, r)
		y = kp(phi, r)./(1 - kp(phi, r));
	end
	
	function y = f(phi, r)
		y = sin(phi)./(1 - af(phi, r)) - cos(phi).*(1 - kp(phi, r))./(tsr * r/rtip);
	end
		
	function y = fpb(phi, r)
		y = sin(phi)./(1 - k(phi, r)) - cos(phi).*(1 - kp(phi, r))./(tsr * r/rtip);
	end	
end
	
