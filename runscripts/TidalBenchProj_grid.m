% This script generates the power and torque curves for the Tidal Benchmarking
% Project turbine, using the polars provided for clean flow (I_\infty = 0.1%)
% These are then saved in the folder ./data-out/ in the format specified by the
% project
% No user input is necessary

clear;
close all;

addpath('../functions', '../classes');

% number of sections to discretise the blade into
bladeSections = 100;

%% SETUP SIMULATION
fileGeom = '../data/TidalBenchProj/geometry-table';
fileAero = '../data/TidalBenchProj/aero-table-grid';
turb = TurbProps(fileGeom, fileAero);

%% RUN SIMULATION
numCases = length(dir("../data/TidalBenchProj/cases/Grid_*.csv"));

% initialise
forces = nan(bladeSections, 7, numCases);
[tsr, cp, ct] = deal(nan(numCases, 1));

% run all cases
nb = 0;
for jj = 1:numCases
	fileOper = sprintf('../data/TidalBenchProj/cases/Grid_%02d', jj);
	run = RunConditions(fileOper);
	sim = TidalSim(run, turb);

	% edit properties to match specifications
	sim.Density = 999.4;
	sim.BladeSections = bladeSections;
	sim.Rotations = 250;
	sim.RotationalAugmentation = false;

	% run the simulation
	sim.RunSimulation;

	% save adimensional coefficients
	tsr(jj) = run.TipSpeedRatio;
	cp(jj) = mean(sum(sim.Power, 1), 2)/(1/2 * sim.Density * run.HubVelocity^3 * pi * sim.Radius^2);
	ct(jj) = mean(sum(sim.Thrust, 1), 2)/(1/2 * sim.Density * run.HubVelocity^2 * pi * sim.Radius^2);

	% package forces for output
	forces(:, 1, jj) = (sim.RadialCoords/sim.Radius).';
	forces(:, 2:7, jj) = [...
		mean(sim.ForceNormal(1, :, :), 3).', ...     Fx
		mean(sim.ForceTangential(1, :, :), 3).', ... Fy
		nan(sim.BladeSections, 1), ...               Fz (not available)
		mean(sim.EdgeBM(1, :, :), 3).', ...          Mx (EBM)
		mean(sim.RootBM(1, :, :), 3).', ...          My (RBM)
		nan(sim.BladeSections, 1), ...               Mz (not available)
		];

	fprintf(repmat('\b', 1, nb));
	nb = fprintf('%2d/%d\n', jj, numCases);
end
rmpath('../functions', '../classes');

% save out
save('./out-mat/out_grid', 'tsr', 'cp', 'ct', 'forces');
