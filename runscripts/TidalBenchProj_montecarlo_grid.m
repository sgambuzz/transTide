% This script runs Monte Carlo simulations by randomly varying the lift and drag
% of all polars by a certain given ΔCl or ΔCd, to compute the confidence
% interval on the computed loads
% The ΔCx are randomly selected from a normal distribution with given stdev and
% are different for each section of the blade and constant with α.
% No user input is necessary

clear;
close all;

addpath('../functions', '../classes');

% number of sections to discretise the blade into
bladeSections = 30;

% settings of Monte Carlo Simulations
nMC = 150; % number of Monte Carlo simulations
sigma_cl = 0.14;
sigma_cd = 0.009;

%% SETUP SIMULATION
fileGeom = '../data/TidalBenchProj/geometry-table';
fileAero = '../data/TidalBenchProj/aero-table-montecarlo';
nCases = length(dir("../data/TidalBenchProj/cases/Grid_*.csv"));

forces = zeros(bladeSections, 7, nCases, nMC);
tsr = zeros(nCases, 1);
[cp, ct] = deal(zeros(nCases, nMC));

%% RUN SIMULATION
for iMC = 1:nMC
	% make noisy polars
	polars = dir('../data/TidalBenchProj/polars/grid/section_*');
	for iSec = 1:length(polars)
		polar = readtable([polars(iSec).folder, filesep, polars(iSec).name]);
		delta_cl = sigma_cl * randn(1, 1);
		delta_cd = sigma_cd * randn(1, 1);
		polar.Cl = polar.Cl + delta_cl;
		polar.Cd = polar.Cd + delta_cd;
		folderout = strrep(polars(iSec).folder, 'grid', 'montecarlo');
		writetable(polar, [folderout, filesep, polars(iSec).name]);
	end

	% run the actual simulation
	turb = TurbProps(fileGeom, fileAero);
	for iCase = 1:nCases
		fileOper = sprintf('../data/TidalBenchProj/cases/Grid_%02d', iCase);
		run = RunConditions(fileOper);
		sim = TidalSim(run, turb);
	
		% edit properties to match specifications
		sim.Density = 999.4;
		sim.BladeSections = bladeSections;
		sim.Rotations = 70;
		sim.RotationalAugmentation = true;
		
		% run the simulation
		sim.RunSimulation;
	
		% save tsr, cp, ct
		tsr(iCase) = run.TipSpeedRatio;
		cp(iCase, iMC) = mean(sum(sim.Power, 1), 2)/(1/2 * sim.Density * run.HubVelocity^3 * pi * sim.Radius^2);
		ct(iCase, iMC) = mean(sum(sim.Thrust, 1), 2)/(1/2 * sim.Density * run.HubVelocity^2 * pi * sim.Radius^2);

		% package for output
		forces(:, 1, iCase, iMC) = (sim.RadialCoords/sim.Radius).';
		forces(:, 2:7, iCase, iMC) = [...
			mean(sim.ForceNormal(1, :, :), 3).', ...     Fx
			mean(sim.ForceTangential(1, :, :), 3).', ... Fy
			nan(sim.BladeSections, 1), ...               Fz (not available)
			mean(sim.EdgeBM(1, :, :), 3).', ...          Mx (EBM)
			mean(sim.RootBM(1, :, :), 3).', ...          My (RBM)
			nan(sim.BladeSections, 1), ...               Mz (not available)
			];
	end
	progress(iMC/nMC);
end
rmpath('../functions', '../classes');

% save outputs (in Matlab form)
save('./out-mat/out_montecarlo_grid', 'forces', 'tsr', 'cp', 'ct', 'sigma_cl', 'sigma_cd');

