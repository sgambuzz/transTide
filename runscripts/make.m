% make folders for storing outputs
if ~exist('./out-mat/', 'dir')
	mkdir('./out-mat/');
end
if ~exist('./out-txt/', 'dir')
	mkdir('./out-txt');
end

% delete previous results
delete('./out-mat/*.mat');
delete('./out-txt/*');

% run the testcases
run('TidalBenchProj_clean.m'); % Low turbulence simulations
run('TidalBenchProj_grid.m'); % Elevated turbulence simulations

% run the Monte Carlo simulations
run('TidalBenchProj_montecarlo_clean.m');
run('TidalBenchProj_montecarlo_grid.m');

% make the output files for submission
run('MakeTextFiles.m');
