clear;
close all;

data = load('./out-mat/out_montecarlo.mat');
data.r = data.out(:, 1);
data.r = data.r/max(data.r);

def = load('plotDefaults.mat');

%% PLOT 1
fh = figure(def.figure{:});
fh.Position(3) = 2*fh.Position(3);
ax(1) = axes(def.axes{:}, 'outerposition', [0, 0, 0.5, 1]);
ax(2) = axes(def.axes{:}, 'outerposition', [0.5, 0, 0.5, 1]);

% plot CP and error bars
fh.CurrentAxes = ax(1);
meancp = mean(data.cp, 2);
stdcp = std(data.cp, [], 2);
fillarea = [[data.tsr(:); flipud(data.tsr(:))], [meancp+2*stdcp; flipud(meancp-2*stdcp)]];
ph = patch('xdata', fillarea(:, 1), 'ydata', fillarea(:, 2), 'facecolor', '#ffdddd', 'edgecolor', 'none', ...
	'facealpha', 0.2, 'displayname', '95 \% confidence interval');
h(1) = plot(data.tsr, meancp, 'k', 'displayname', 'Mean power');
lh = plot(data.tsr, meancp+2*stdcp, 'r', 'linewidth', 0.5);
plot(data.tsr, meancp-2*stdcp, 'color', lh.Color, 'linewidth', lh.LineWidth);
h(2) = patch('xdata', [nan, nan], 'ydata', [nan, nan], 'facecolor', ph.FaceColor', 'edgecolor', lh.Color, ...
	'facealpha', ph.FaceAlpha, 'displayname', ph.DisplayName);
xlabel('$\lambda$');
ylabel('$C_P$');
legend(h, def.text{:}, 'location', 'sw');

% plot CT and error bars
fh.CurrentAxes = ax(2);
meanct = mean(data.ct, 2);
stdct = std(data.ct, [], 2);
fillarea = [[data.tsr(:); flipud(data.tsr(:))], [meanct+2*stdct; flipud(meanct-2*stdct)]];
patch('xdata', fillarea(:, 1), 'ydata', fillarea(:, 2), 'facecolor', '#ffdddd', 'edgecolor', 'none', ...
	'facealpha', 0.2, 'displayname', '95 \% confidence interval');
plot(data.tsr, meanct, 'k', 'displayname', 'Mean power');
plot(data.tsr, meanct+2*stdct, 'color', lh.Color, 'linewidth', lh.LineWidth);
plot(data.tsr, meanct-2*stdct, 'color', lh.Color, 'linewidth', lh.LineWidth);
xlabel('$\lambda$');
ylabel('$C_T$');

%% PLOT 2
fh = figure(def.figure{:});
fh.Position(3) = 2*fh.Position(3);
ax(1) = axes(def.axes{:}, 'outerposition', [0, 0, 0.5, 1]);
ax(2) = axes(def.axes{:}, 'outerposition', [0.5, 0, 0.5, 1]);

% plot pct error
fh.CurrentAxes = ax(1);
plot(data.tsr, 2*stdcp./meancp*100, 'k');
xlabel('$\lambda$');
ylabel('$$\frac{\epsilon_{C_P}}{C_P}$$ $[\times 10^{-2}]$');

fh.CurrentAxes = ax(2);
plot(data.tsr, 2*stdct./meanct*100, 'k');
xlabel('$\lambda$');
ylabel('$$\frac{\epsilon_{C_T}}{C_T}$$ $[\times 10^{-2}]$');

%% PLOT 3
fh = figure(def.figure{:});
fh.Position = [1, 1, 2, 1.5] .* fh.Position;
t = tiledlayout(2, 2, def.tiledlayout{:});
xlabel(t, '$r/R$', def.text{:});
tsr = 5.5; % best-case scenario
[~, ix] = min(abs(tsr - data.tsr));
for jj = [1, 2, 4, 5]
	ax(jj) = nexttile;
	set(ax(jj), def.axes{:});
	meanload = mean(data.out(:, jj+1, ix, :), 4);
	stdload = std(data.out(:, jj+1, ix, :), [], 4);
	fillarea = [[data.r(:); flipud(data.r(:))], [meanload+2*stdload; flipud(meanload-2*stdload)]];
	patch('xdata', fillarea(:, 1), 'ydata', fillarea(:, 2), 'facecolor', '#ffdddd', 'edgecolor', 'none', ...
		'facealpha', 0.2, 'displayname', '95 \% confidence interval');
	plot(data.r, meanload, 'color', 'k');
	plot(data.r, meanload+2*stdload, 'color', lh.Color, 'linewidth', lh.LineWidth);
	plot(data.r, meanload-2*stdload, 'color', lh.Color, 'linewidth', lh.LineWidth);
end
ylabel(ax(1), '$F_x$', def.text{:});
ylabel(ax(2), '$F_y$', def.text{:});
ylabel(ax(4), '$M_x$', def.text{:});
ylabel(ax(5), '$M_y$', def.text{:});

%% PLOT 4
fh = figure(def.figure{:});
fh.Position = [1, 1, 2, 1.5] .* fh.Position;
t = tiledlayout(2, 2, def.tiledlayout{:});
xlabel(t, '$r/R$', def.text{:});
ix = 7;
for jj = [1, 2, 4, 5]
	ax(jj) = nexttile;
	set(ax(jj), def.axes{:});
	meanload = mean(data.out(:, jj+1, ix, :), 4);
	stdload = std(data.out(:, jj+1, ix, :), [], 4);
	plot(data.r, 2*stdload./meanload*100, 'k');
	xlim([0.1, 0.9]);
end
ylabel(ax(1), '$$\frac{\epsilon_{F_x}}{F_x}$$ $[\times 10^{-2}]$', def.text{:});
ylabel(ax(2), '$$\frac{\epsilon_{F_y}}{F_y}$$ $[\times 10^{-2}]$', def.text{:});
ylabel(ax(4), '$$\frac{\epsilon_{M_x}}{M_x}$$ $[\times 10^{-2}]$', def.text{:});
ylabel(ax(5), '$$\frac{\epsilon_{M_y}}{M_y}$$ $[\times 10^{-2}]$', def.text{:});
