%% clean cases (Low turbulence cases)
data = load('./out-mat/out_clean.mat');

% distributions
folder_out = './out-txt/Low Turbulence Cases/';
if ~exist(folder_out, 'dir')
	mkdir(folder_out);
end
header_master = fileread('./headers/header_dists.txt');
ncases = size(data.forces, 3);
for icase = 1:ncases
	fname = [folder_out, sprintf('forcemoment_dists_clean_%03d.txt', icase)];
	
	% write the header
	header = strrep(header_master, '$CASENR', sprintf('Clean %d', icase));
	write_header(header, fname);

	% write the blade-resolved forces
	out = data.forces(:, :, icase);
	out(isnan(out)) = 0;
	out = num2str(out, '% 12.5f\t');
	writematrix(out, fname, 'writemode', 'append');
end

% integrated vars
header_master = fileread('./headers/header_integrated.txt');
fname = [folder_out, 'integrated_vars.txt'];

% compute the blade-wise integrals
integ = trapz(data.forces(:, 1, 1), data.forces(:, 2:end, :), 1);
integ = permute(integ, [3, 2, 1]);
integ = cat(2, integ, data.ct, data.cp);
integ(isnan(integ)) = 0;

% write the header
header = strrep(header_master, '$TURB', 'Low');
write_header(header, fname);

% write the data
integ = string(num2str(integ, "% 12.5f\t"));
for irow = 1:length(integ)
	integ(irow) = sprintf("Case% 3d\t", irow) + integ(irow);
end
integ = char(integ);
writematrix(integ, fname, 'writemode', 'append');

%% Grid cases (Elevated turbulence cases)
data = load('./out-mat/out_grid.mat');

% distributions
folder_out = './out-txt/Elevated Turbulence Cases/';
if ~exist(folder_out, 'dir')
	mkdir(folder_out);
end
header_master = fileread('./headers/header_dists.txt');
ncases = size(data.forces, 3);
for icase = 1:ncases
	fname = [folder_out, sprintf('forcemoment_dists_grid_%03d.txt', icase)];
	
	% write the header
	header = strrep(header_master, '$CASENR', sprintf('Grid %d', icase));
	write_header(header, fname);

	% write the blade-resolved forces
	out = data.forces(:, :, icase);
	out(isnan(out)) = 0;
	out = num2str(out, '% 12.5f\t');
	writematrix(out, fname, 'writemode', 'append');
end

% integrated vars
header_master = fileread('./headers/header_integrated.txt');
fname = [folder_out, 'integrated_vars.txt'];

% compute the blade-wise integrals
integ = trapz(data.forces(:, 1, 1), data.forces(:, 2:end, :), 1);
integ = permute(integ, [3, 2, 1]);
integ = cat(2, integ, data.ct, data.cp);
integ(isnan(integ)) = 0;

% write the header
header = strrep(header_master, '$TURB', 'Elevated');
write_header(header, fname);

% write the data
integ = string(num2str(integ, "% 12.5f\t"));
for irow = 1:length(integ)
	integ(irow) = sprintf("Case% 3d\t", irow) + integ(irow);
end
integ = char(integ);
writematrix(integ, fname, 'writemode', 'append');

%% Uncertainties on clean
data = load('./out-mat/out_montecarlo_clean.mat');

% distribution
ix = 6; % reference test case for uncertainty estimation
folder_out = './out-txt/Low Turbulence Cases/Uncertainty/';
if ~exist(folder_out, 'dir')
	mkdir(folder_out);
end

% write the header
header_master = fileread('./headers/header_uncertainty_dists.txt');
header = strrep(header_master, '$TESTCASE', sprintf('Clean %d', ix));
fname = [folder_out, sprintf('forcemoment_dists_uncertainty_clean_%03d.txt', ix)];
write_header(header, fname);

% write the distributions
r = data.forces(:, 1);
r = r/max(r);
out = std(data.forces(:, 2:end, ix, :), [], 4);
out(isnan(out)) = 0;
out = [r, out];
out = num2str(out, '% 12.5f\t');
writematrix(out, fname, 'writemode', 'append');

% integrated vars
header_master = fileread('./headers/header_uncertainty_integrated.txt');
header = strrep(header_master, '$TURB', 'Low');
fname = [folder_out, 'integrated_vars_uncertainty.txt'];
write_header(header, fname);

integ = trapz(data.forces(:, 1, 1, 1), data.forces(:, 2:end, :, :), 1);
unc = std(integ, [], 4);
unc = permute(unc, [3, 2, 1]);
unc(isnan(unc)) = 0;
unc = num2str(unc, '% 12.5f\t');
writematrix(unc, fname, 'writemode', 'append');

%% Uncertainties on grid data
data = load('./out-mat/out_montecarlo_grid.mat');

% distribution
ix = 7; % reference test case for uncertainty estimation
folder_out = './out-txt/Elevated Turbulence Cases/Uncertainty/';
if ~exist(folder_out, 'dir')
	mkdir(folder_out);
end

% write the header
header_master = fileread('./headers/header_uncertainty_dists.txt');
header = strrep(header_master, '$TESTCASE', sprintf('Grid %d', ix));
fname = [folder_out, sprintf('forcemoment_dists_uncertainty_turb_%03d.txt', ix)];
write_header(header, fname);

% write the distributions
r = data.forces(:, 1);
r = r/max(r);
out = std(data.forces(:, 2:end, ix, :), [], 4);
out(isnan(out)) = 0;
out = [r, out];
out = num2str(out, '% 12.5f\t');
writematrix(out, fname, 'writemode', 'append');

% integrated vars
header_master = fileread('./headers/header_uncertainty_integrated.txt');
header = strrep(header_master, '$TURB', 'Elevated');
fname = [folder_out, 'integrated_vars_uncertainty.txt'];
write_header(header, fname);

integ = trapz(data.forces(:, 1, 1, 1), data.forces(:, 2:end, :, :), 1);
unc = std(integ, [], 4);
unc = permute(unc, [3, 2, 1]);
unc(isnan(unc)) = 0;
unc = num2str(unc, '% 12.5f\t');
writematrix(unc, fname, 'writemode', 'append');


%% FUNCTIONS
function write_header(header, fname)
	fid = fopen(fname, 'w');
	fprintf(fid, '%s', header);
	fclose(fid);
end
