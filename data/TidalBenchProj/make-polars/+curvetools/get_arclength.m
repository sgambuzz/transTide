function s = get_arclength(c)
% given a sorted curve, get the arclength s
d = diff(c, 1);
s = [0; cumsum(sqrt(sum(d.^2, 2)))];
