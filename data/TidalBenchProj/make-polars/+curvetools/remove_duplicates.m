function c = remove_duplicates(c)
% given a curve with non-unique points, keeps the unique ones without
% sorting the curve points
[~, ix] = unique(c, 'rows');
ix = sort(ix);
c = c(ix, :);
