function c = reinterpolate(c, N, varargin)
% reinterpolate the points of the curve `c` to be regularly-spaced
if nargin < 2 || isempty(N)
	N = length(c);
end

c = curvetools.remove_duplicates(c);
s = curvetools.get_arclength(c);
snew = linspace(s(1), s(end), N);
c = interp1(s, c, snew, varargin{:});

