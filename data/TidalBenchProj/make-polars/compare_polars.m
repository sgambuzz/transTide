% plot the polars computed via xfoil and the ones given as refrence (RANS) to
% compare and estimate uncertainty.

clear;
close all;

% clean
tab = readtable('../aero-table-clean.csv');
[~, ix] = min(abs(tab.rad_coord - 0.8*tab.rad_coord(end)));
fname = "../" + tab.foil{ix};
polar_clean = readtable(fname + ".csv");
ref = readtable("../polars/reference/NACA_63415_TI_00pct.csv");

def = load('plotDefaults.mat');
fh1 = figure(def.figureppt{:});
fh1.Position(3) = fh1.Position(3)*2;
ax(1) = axes(def.axes{:}, 'outerposition', [0, 0, 0.5, 1], 'colororder', brewermap([], 'set1'));
ax(2) = axes(def.axes{:}, 'outerposition', [0.5, 0, 0.5, 1], 'colororder', brewermap([], 'set1'));

fh1.CurrentAxes = ax(1);
plot(rad2deg(polar_clean.aoa), polar_clean.Cl, 'displayname', 'Computed with XFoil ($n$ = 9, free transition)');
plot(rad2deg(ref.aoa), ref.Cl, 'displayname', 'Reference');
xlabel('$\alpha$ [deg]');
ylabel('$C_l$');
legend(def.textsml{:}, 'location', 'se');

fh1.CurrentAxes = ax(2);
plot(rad2deg(polar_clean.aoa), polar_clean.Cd, 'displayname', 'Computed with XFoil ($n$ = 9, free transition)');
plot(rad2deg(ref.aoa), ref.Cd, 'displayname', 'Reference');
xlabel('$\alpha$ [deg]');
ylabel('$C_d$');

% grid
tab = readtable('../aero-table-grid.csv');
[~, ix] = min(abs(tab.rad_coord - 0.8*tab.rad_coord(end)));
fname = "../" + tab.foil{ix};
polar_grid = readtable(fname + ".csv");
ref = readtable("../polars/reference/NACA_63415_TI_01pct.csv");

def = load('plotDefaults.mat');
fh2 = figure(def.figureppt{:});
fh2.Position(3) = fh2.Position(3)*2;
ax(1) = axes(def.axes{:}, 'outerposition', [0, 0, 0.5, 1], 'colororder', brewermap([], 'set1'));
ax(2) = axes(def.axes{:}, 'outerposition', [0.5, 0, 0.5, 1], 'colororder', brewermap([], 'set1'));

fh2.CurrentAxes = ax(1);
plot(rad2deg(polar_grid.aoa), polar_grid.Cl, 'displayname', 'Computed with XFoil ($n$ = 3.5, free transition)');
plot(rad2deg(ref.aoa), ref.Cl, 'displayname', 'Reference');
xlabel('$\alpha$ [deg]');
ylabel('$C_l$');
legend(def.textsml{:}, 'location', 'se');

fh2.CurrentAxes = ax(2);
plot(rad2deg(polar_grid.aoa), polar_grid.Cd, 'displayname', 'Computed with XFoil ($n$ = 3.5, free transition)');
plot(rad2deg(ref.aoa), ref.Cd, 'displayname', 'Reference');
xlabel('$\alpha$ [deg]');
ylabel('$C_d$');
