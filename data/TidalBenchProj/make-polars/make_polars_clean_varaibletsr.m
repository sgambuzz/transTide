% make polars for the BEM code assuming dependency of the chord Reynolds both on
% the r/R and the tip-speed ratio
% clean: set n = 9, no forced transition

clear;
close all;

%% input
nstations = 29;

% xfoil options
xfoilopts = {...
	'oper iter 200', ...
	'oper vpar n 9', ...
	'ppar n 300', ...
	'norm', ...
	};
alpha = -10:1:15;

% constants
rtip = 0.8;
uinf = 1;
rho = 999.4; % kg/m3
mu = 1.2160e-3; % kg/(m s)

foil = readmatrix('./foil.txt');
foil = curvetools.reinterpolate(foil, 495);

turb = readmatrix('../geometry-table.csv');
turb = turb(:, 3:end);
turb(:, 1) = turb(:, 1) * rtip; % convert to dimensional
r = linspace(turb(1, 1), turb(end, 1), nstations);
c = interp1(turb(:, 1), turb(:, 3), r);

%% get tsr list
files = dir("../cases/Clean_*.csv");
tsrlist = zeros(size(files));
for jj = 1:length(files)
	temp = readtable("../cases/" + files(jj).name);
	tsrlist(jj) = temp.TSR;
end

%%
for ii = 1:length(tsrlist)
	% compute local Re
	tsr = tsrlist(ii);	
	omega = tsr * uinf / rtip;
	lsr = tsr * r/rtip;
	
	% compute u_ax and u_az assuming induction factors are ideal
	a = 1/3 * ones(size(r));
	ap = a.*(1-a)./(lsr.^2);
	u_ax = uinf * (1-a);
	u_az = omega * r .* (1+ap);
	utot = hypot(u_ax, u_az);
	
	rec = rho * utot .* c / mu; % rec(icase, isection)
	
	% compute with xfoil, and package for transTide
	header = '           aoa,              Cl,              Cd,              Cn,              Cc,         aoa_Cl0,    Cl_lin_slope,    Cl_lin_range,          Cd_Cl0,              Re,';
	
	for jj = 1:length(rec)
		p = xfoil_robust(foil, alpha, rec(jj), 0, xfoilopts{:});

		if isempty(p)
			% xfoil has hung for more than 10 seconds, we have no data
			continue
		end

		% fill missing
		p = table2array(p);
		p = interp1(p(:, 1), p(:, [1:3, 5]), alpha, 'linear', 'extrap');
		
		mask = p(:, 1) > -5 & p(:, 1) < 5; % assuming linear range
		
		% package
		out = nan(length(alpha), 10);
		out(:, 1) = deg2rad(p(:, 1));                                           % aoa
		out(:, 2) = p(:, 2);                                                    % Cl
		out(:, 3) = p(:, 3);                                                    % Cd
		out(:, 4) =  out(:, 2) .* cos(out(:, 1)) + out(:, 3) .* sin(out(:, 1)); % Cn
		out(:, 5) = -out(:, 2) .* sin(out(:, 1)) + out(:, 3) .* cos(out(:, 1)); % Cc
	
		poly = polyfit(out(mask, 1), out(mask, 2), 1);
		out(1, 6) = interp1(out(mask, 2), out(mask, 1), 0, 'linear', 'extrap'); % aoa_cl0
		out(1, 7) = poly(1);                                                    % Cl_lin_slo
		out(1:2, 8) = deg2rad([-5; 10]);                                        % Cl_lin_range
		out(1, 9) = interp1(out(mask, 2), out(mask, 3), 0, 'linear', 'extrap'); % Cd_Cl0
		out(1, 10) = rec(jj);                                                   % Re
	
		% save
		fname = sprintf('../polars/clean/section_%02d_tsr_%.0f.csv', jj, tsrlist(ii)*100);
		saveascii(header, fname);
		saveascii(out, fname, '% 14.6f, ', 'a');
	
		% remove the NaNs
		writelines(strrep(fileread(fname), 'NaN', '   '), fname);
	end
	
	%% make aero table
	aero = [];
	aero.rad_coord = r.';
	aero.foil = strsplit(sprintf('polars/clean/section_%02d\n', 1:length(r)), newline);
	aero.foil = aero.foil + sprintf("_tsr_%.0f", tsrlist(ii)*100);
	aero.foil = aero.foil(1:end-1).';
	aero = struct2table(aero);
	writetable(aero, sprintf('../aero_table_clean_tsr_%.0f.csv', tsrlist(ii)*100));
end
