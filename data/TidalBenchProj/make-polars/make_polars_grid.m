% making the polars for the clean test case, assuming Re of design operation
% and changing Re along the blade span

% grid: set n = n(ti_equiv), no forced transition
% ti_equiv is computed assuming u' and v' are random signals with given std

clear;
close all;

%% input
nstations = 29;

% xfoil options
xfoilopts = {...
	'oper iter 200', ...
	'ppar n 300', ...
	'norm', ...
	};
alpha = -10:1:15;

%% prelim
% design
tsr = 6;
rtip = 0.8;
ti = 3; % pct

% operating
uinf = 0.9207;
rho = 999.4; % kg/m3
mu = 1.2160e-3; % kg/(m s)
omega = tsr * uinf / rtip;

% load from file
foil = readmatrix('./foil.txt');
foil = curvetools.reinterpolate(foil, 495);
turb = readtable('../geometry-table.csv');
turb = table2array(turb(:, 3:end));

% compute local Re
r = linspace(turb(1, 1), turb(end, 1), nstations);
c = interp1(turb(:, 1), turb(:, 3), r);
lsr = tsr * r/rtip;

% compute u_ax and u_az assuming induction factors are ideal
up = ti/100 * uinf * randn(1e4, 1); % signal having no mean and given std

a = 1/3 * ones(size(r));
ap = a.*(1-a)./(lsr.^2);
u_ax = uinf * (1-a) + up;
u_az = omega * r .* (1+ap) + up;
utot = sqrt(u_ax.^2 + u_az.^2);

umean = mean(utot, 1);
ustd = std(utot, 1);
ti_r = ustd./umean;

rec = rho * umean .* c / mu;

%% compute with xfoil, and package for transTide
header = '           aoa,              Cl,              Cd,              Cn,              Cc,         aoa_Cl0,    Cl_lin_slope,    Cl_lin_range,          Cd_Cl0,              Re,';

for jj = 1:length(rec)
	n = -8.43 - 2.4 * log(ti_r(jj));
	n = max(0.5, min(n, 9)); % bound to [0.5, 9]

	p = xfoil(foil, alpha, rec(jj), 0, xfoilopts{:}, sprintf('oper vpar n %.2f', n));
	
	% fill missing
	p = table2array(p);
	p = interp1(p(:, 1), p(:, [1:3, 5]), alpha, 'linear', 'extrap');
	
	mask = p(:, 1) > -5 & p(:, 1) < 5; % assuming linear range
	
	% package
	out = nan(length(alpha), 10);
	out(:, 1) = deg2rad(p(:, 1));                                           % aoa
	out(:, 2) = p(:, 2);                                                    % Cl
	out(:, 3) = p(:, 3);                                                    % Cd
	out(:, 4) =  out(:, 2) .* cos(out(:, 1)) + out(:, 3) .* sin(out(:, 1)); % Cn
	out(:, 5) = -out(:, 2) .* sin(out(:, 1)) + out(:, 3) .* cos(out(:, 1)); % Cc

	poly = polyfit(out(mask, 1), out(mask, 2), 1);
	out(1, 6) = interp1(out(mask, 2), out(mask, 1), 0);                     % aoa_cl0
	out(1, 7) = poly(1);                                                    % Cl_lin_slo
	out(1:2, 8) = deg2rad([-5; 10]);                                        % Cl_lin_range
	out(1, 9) = interp1(out(mask, 2), out(mask, 3), 0);                     % Cd_Cl0
	out(1, 10) = rec(jj);                                                   % Re

	% save
	fname = sprintf('../polars/grid/section_%02d.csv', jj);
	saveascii(header, fname);
	saveascii(out, fname, '% 14.6f, ', 'a');

	% remove the NaNs
	writelines(strrep(fileread(fname), 'NaN', '   '), fname);
end

%% make aero table
aero = [];
aero.rad_coord = r.';
aero.foil = strsplit(sprintf('polars/grid/section_%02d.csv\n', 1:length(r)), newline);
aero.foil = aero.foil(1:end-1).';
aero = struct2table(aero);
writetable(aero, '../aero-table-grid.csv');

